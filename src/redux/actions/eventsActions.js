import axios from 'axios';
import api from 'api';

export function getEventsList() {
    return {
        type: 'GET_EVENTS',
        payload: axios.get(api.getData('events'))
            .then(response => response.data)
            .catch(error => error)
    }
}