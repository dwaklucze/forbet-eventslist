import React from 'react';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';
import loggerMiddleware from 'redux-logger';

import eventsReducer from 'redux/reducers/eventsReducer';

const middleware = applyMiddleware(
    thunkMiddleware,
    promiseMiddleware(),
    loggerMiddleware
);

const store = createStore(
    combineReducers({
        eventsReducer
    }),
    middleware
);

export default store;