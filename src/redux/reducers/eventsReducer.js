import { sortBy } from 'lodash';

const initialState = {
    fetching: false,
    fetched: false,
    data: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_EVENTS_PENDING':
            return {
                ...state,
                fetching: true
            }
            break;
        case 'GET_EVENTS_REJECTED':
            return {
                ...state,
                fetching: false,
                fetched: false,
                error: action.payload
            }
            break;
        case 'GET_EVENTS_FULFILLED':
            const data = sortBy(action.payload.data, (o) => { return o.eventStart })
            return {
                ...state,
                fetched: true,
                fetching: false,
                error: false,
                data
            }
            break;
        default:
            return { ...state };
            break;
    };
}

export default reducer;