export default {
    paths: {
        api: "https://www.iforbet.pl/rest/",
        data: {
            events: "market/categories/multi/2432/events"
        }
    },
    getData(attr) {
        return `${this.paths.api}${this.paths.data[attr]}`
    }
}