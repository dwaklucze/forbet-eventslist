import React, { Component } from 'react';
import { Provider } from 'react-redux';
import styled from 'styled-components';

import Events from 'components/Events';
import store from 'redux/store';

const AppWrapper = styled.div`
    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Helvetica,Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
    font-size: .875rem;
    line-height: 1.5;
    * {
        box-sizing: border-box;
    }
`;

export class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <AppWrapper>
                    <Events />
                </AppWrapper>
            </Provider>
        )
    }
}
