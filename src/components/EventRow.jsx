import React, { Component } from 'react';
import { EventGameRow } from 'components/EventGameRow';
import moment from 'moment';
import styled from 'styled-components';

const EventWrapper = styled.div` 
    background: linear-gradient(0deg, #f3f3f3, #f3f3f3);
    border-bottom: ${props => props.isSeparated ? 1 : 0}px solid #619641;
`
const EventHeader = styled.div`
    display: flex; 
    flex-direction: row;
    padding: 0 10px;
`
const EventInfo = styled.div`
    line-height: 1.5rem;
    font-size: 12px;
    color: #565555;
    flex: 1;
`
const EventTitle = styled.div`
    height: 1.5rem;
    line-height: 1.5rem;
    color: #003c7f;
    font-size: 14px;
    width: 100%;
    flex: 1;
`
const EventGamesCount = styled.div`
display: flex;
justify-content: center;
align-items: center;
color: #619641;
`

export class EventRow extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    _parseEventToState(event) {
        return {
            time: moment(event.eventStart).format('DD.MM HH:mm'),
            name: event.eventName,
            category: event.category1Name,
            category2: event.category2Name,
            category3: event.category3Name,
            games: event.eventGames,
            type: event.eventType,
            gamesCount: event.gamesCount,
            remoteId: event.remoteId
        }
    }

    componentWillMount() {
        this.setState(this._parseEventToState(this.props.data))
    }

    render() {
        const event = this.state;
        return (
            <EventWrapper isSeparated={!event.remoteId}>
                <EventHeader>
                    <EventInfo>
                        <div>{event.time} {event.category}</div>
                        <EventTitle>{event.name}</EventTitle>
                    </EventInfo>
                    <EventGamesCount>+{event.gamesCount}</EventGamesCount>
                </EventHeader>

                {event.remoteId ? <EventGameRow data={event.games} /> : null}
            </EventWrapper>
        );
    }
}