import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { getEventsList } from 'redux/actions/eventsActions';
import { EventRow } from 'components/EventRow';

class Events extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.dispatch(getEventsList());
    }

    render() {
        const { events } = this.props;
        return (
            <div>
                {events.fetched && events.data.map((event, k) => <EventRow key={k} data={event}></EventRow>)}
                {events.fetching && 'Trwa ładowanie zakładów, proszę czekać...'}
                {events.error && 'Wystąpił błąd podczas ładowania zakładów...'}
                {(events.fetched && events.data.length === 0) && 'Brak zakładów.'}
            </div>
        );
    }
}

const mapStateToProps = (state) => ({ events: state.eventsReducer })
export default connect(mapStateToProps)(Events);