import React, { Component } from 'react';
import styled from 'styled-components';

const EventGameRowWrapper = styled.div`
    display: flex;
    flex: 1;
    flex-direction: column;
    background: white;
    border-top: 1px solid #c0d6ec;
    border-bottom: 1px solid #c0d6ec;
`;
const EventGameRowName = styled.div`
    line-height: 1.5rem;
    color: #003c7f;
`
const OutcomeWrapper = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
`
const OutcomeRow = styled.div`
    color: ${props => props.isTitle ? '#003c7f' : 'initial'};
    flex: 1;
    text-align: center;
    border-right: 1px solid #c0d6ec;
    &:last-child {
        border-right: 0;
    }
`

export class EventGameRow extends Component {

    constructor(props) {
        super(props);
    }

    _renderOutcome(game, displayKey, isTitle) {
        return game.outcomes.map((outcome, k) => <OutcomeRow key={k} isTitle={isTitle}>
            {outcome[displayKey]}
        </OutcomeRow>)
    }

    render() {
        const { data } = this.props;

        return (
            data.map((game, gameKey) => <EventGameRowWrapper key={gameKey}>
                <OutcomeWrapper>
                    {this._renderOutcome(game, 'outcomeName', 'true')}
                </OutcomeWrapper>
                <OutcomeWrapper>
                    {this._renderOutcome(game, 'outcomeOdds', 'false')}
                </OutcomeWrapper>
            </EventGameRowWrapper>)
        );
    }
}
