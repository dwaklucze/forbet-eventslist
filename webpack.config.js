const path = require('path');
const webpack = require('webpack');
module.exports = {
  devtool: 'source-map',
  context: path.resolve(__dirname, 'src'),
  entry: ['babel-polyfill', 'react-hot-loader/patch', './main.js'],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'public')
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)?$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  },
  resolve: {
    modules: [
      path.join(__dirname, 'node_modules'),
      path.join(__dirname, 'src')
    ],
    extensions: ['.js', '.jsx']
  },
  devServer: {
    contentBase: path.join(__dirname, "public"),
    port: 7777,
    publicPath: '/'
  },
  plugins: [
    new webpack.NamedModulesPlugin()
  ]
}
